package application;
import vehicles.Bicycle;

public class Bickestore{
    public static void main(String[] args) {

        Bicycle[] bicke = new Bicycle[4];
        bicke[0] = new Bicycle("Magura", 33, 70);
        bicke[1] = new Bicycle("Manitou", 30, 65);
        bicke[2] = new Bicycle("Marzocchi", 27, 60);
        bicke[3] = new Bicycle("Mavic", 24, 55);

        for(int i = 0; i < bicke.length; i++){
            System.out.println(bicke[i]);
        }
    }
}